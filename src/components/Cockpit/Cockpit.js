import React from 'react';

const cockpit = (props) => {

    const style = {
        backgroundColor: 'white',
        font: 'inherit',
        border: '1px solid blue',
        cursor: 'pointer',
    }

    return (
        <div>
            <h1>{props.title}</h1>
            <p>This is really working!</p>
            <button style={style} onClick={props.switchName}>Switch Name</button>
        </div>
    );
}

export default cockpit;