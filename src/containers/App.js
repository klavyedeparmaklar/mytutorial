import React, { Component } from 'react';

import '../containers/App.css';

import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {
  state = {
    persons: [
      { id: 'asd1', name: 'Sep', age: 26 },
      { id: 'afafw1', name: 'Buket', age: 25 },
      { id: '2311', name: 'Sebo', age: 30 },
    ],
    showPersons: false,
  }

  deletePersonHandler = (personIndex) => {
    //We have to copy the state after that we should manupulate it
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({ persons: persons });
  }

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = { ...this.state.persons[personIndex] };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({ persons: persons });
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  render() {
    let persons = null;

    if (this.state.showPersons) {
      persons = (
        <div>
          <Persons
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangeHandler}
          />
        </div>
      );
    }

    return (
      <div className="App" >
        <Cockpit
          title={this.props.appTitle}
          switchName={this.togglePersonsHandler} />
        {persons}
      </div>
    );
  }
}

export default App;
